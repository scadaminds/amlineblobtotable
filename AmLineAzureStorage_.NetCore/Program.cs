﻿using System.Linq;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Net;
using System.Collections.Concurrent;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading;
using AmLineAzureStorage.Models;
using Microsoft.WindowsAzure.Storage;
using Scm.DataStorage.Azure;
using Scm.Util.Async;
using AmLineAzureStorage.Helpers;
using Microsoft.WindowsAzure.Storage.Table;

namespace AmLineAzureStorage
{
    public class Program
    {
        public static int Main(string[] args)
        {
            //public string BlobName { get; set; }

            return MainAsync(new CancellationTokenSource().Token)?.GetAwaiter().GetResult() ?? -1;
        }

        static async Task<int> MainAsync(CancellationToken cancellationToken)
        {
            var storageAccount = new MyConfig().CloudStorageAccount;

            {
                var blobName = "Sensor1day.csv";
                var cloudBlobClient = storageAccount.CreateCloudBlobClient();
                var cloudBlobContainer = cloudBlobClient.GetContainerReference("csvblob");
                var cloudBlobkBloc = cloudBlobContainer.GetBlockBlobReference(blobName);
                var cloudTableClient = storageAccount.CreateCloudTableClient();
                var cloudTable = cloudTableClient.GetTableReference("PowerBiTestTable"); //("amlineSLMData");
                await cloudTable.CreateIfNotExistsAsync(new TableRequestOptions { }, null, cancellationToken).ConfigureAwait(false);

                var tableServicePoint = ServicePointManager.FindServicePoint(storageAccount.TableEndpoint);
                tableServicePoint.UseNagleAlgorithm = false;
                tableServicePoint.Expect100Continue = false;
                tableServicePoint.ConnectionLimit = 1000;

                var partitionLocation = "TI";
                var partitionEquipment = "SLM";
                var partitionResolution = "Source";
                var lineCount = 100;

                Console.WriteLine("Started job on blob: {0}, reading {1} lines", blobName, lineCount);

                var headerLine = (await cloudBlobkBloc.Lines().FirstAsync()).Split('\r');
                var header = headerLine[0].Split(';').Select(SafeParameter.Replace).ToArray();
                var prefix = partitionLocation + "_" + partitionEquipment;
                var postfix = partitionResolution;
                var entities = ObserveEntities(cloudBlobkBloc.Lines().Skip(1).Take(lineCount), header, prefix, postfix); //.Take(lineCount)
                entities = entities
                    .ScanSumTo(x => 1, cnts => cnts.Sample(TimeSpan.FromSeconds(5)).TimeInterval()
                        .Do(
                            x => Console.WriteLine("Entities: {0} in {1} @ {2}/s", x.Value, x.Interval.TotalSeconds,
                                x.Value / x.Interval.TotalSeconds),
                            () => Console.WriteLine("All Entities")));
                var sw = Stopwatch.StartNew();

                void Progress(long sofar)
                    => Console.WriteLine("Inserted: {0} in {1} @ {2:f0}/s", sofar, sw.Elapsed,
                        sofar / sw.Elapsed.TotalSeconds);

                var entityCount = await cloudTable.InsertOrMerge(entities)
                    //.Select(
                    //    (o, i) => o.Do(_ => Console.WriteLine("Batch {0} ready", i))
                    //        .Select((l, i2) =>
                    //            {
                    //                Console.WriteLine("Batch {0}: {1}", i, l.Count);
                    //                return l;
                    //            })
                    //        .Do(_ => {}, () => Console.WriteLine("Batch {0}: done", i)))
                    .Merge(tableServicePoint.ConnectionLimit) // Control concurrency
                    .ScanSumTo(x => x.Count, cnts => cnts.Sample(TimeSpan.FromSeconds(5)).Do(Progress))
                    .Sum(x => x?.Count ?? 0)
                    .Finally(() => Console.WriteLine("Done"))
                    .ToTask(cancellationToken).ConfigureAwait(false);
                sw.Stop();
                Progress(entityCount);
                Console.WriteLine("Waiting for Enter...");
                //Console.ReadLine();
            }
            return 0;
        }

        public static IObservable<ITableEntity> ObserveEntities(
            IObservable<string> lines,
            string[] inputHeaders, string prefix, string postfix)
        {
            var importHeadersWithIndex = inputHeaders.Select((header, index) => new { header, index })
                //.Where(x => x.header == "Pressure")
                .ToDictionary(x => x.header, x => x.index);
            IEnumerable<string> PickValues(string[] values) => importHeadersWithIndex.Values.Select(i => values[i]);
            IEnumerable<Func<string, string, string, ITableEntity>> parsers =
                importHeadersWithIndex.Select(h =>
                {
                    var name = $"{h.Key}_Sample";
                    if (h.Value == 0)
                        return (part, row, str) => SampleDateTimeEntity.FromString(part, row, name, str);
                    return (Func<string, string, string, ITableEntity>) ((part, row, str) => SampleDoubleEntity.FromString(part, row, name, str));
                })
                .ToArray();

            return lines
                .Select((row, line) =>
                {
                    var value = row.Split(';');
                    if (inputHeaders.Length != value.Length)
                        throw new InvalidOperationException(
                            $"Wrong amount of values, got {value.Length}, needs {inputHeaders.Length} in line {line}");
                    var rowKey = TimeParse.TimeParseMethod(value[0]);
                    return new { value, rowKey };
                })
                // Only the first row for repeated keys
                .GroupBy(x => x.rowKey).SelectMany(grp => grp.FirstOrDefaultAsync()
                    // Map that row in entities
                    .SelectMany(row => importHeadersWithIndex.Keys.Zip(PickValues(row.value), (h, v) => new { h, v })
                    .Zip(parsers, (x, p) => p($"{prefix}_{x.h}_{postfix}", row.rowKey.ToString("o"), x.v))));
        }
    }
}



