using System;
using System.Threading;

namespace AmLineAzureStorage.Helpers
{
    public class ConcurrentCount
    {
        private long _current;
        private long _maxCurrent;

        public interface IScope : IDisposable
        {
            long Current { get; }
            long MaxCurrent { get; }
            int ThreadIdOnEnter { get; }
            int ThreadIdOnExit { get; }
        }
        private sealed class ScopeImpl : IScope
        {
            public long Current { get; }
            public long MaxCurrent { get; }
            public int ThreadIdOnEnter { get; }
            public int ThreadIdOnExit { get; private set; }
            private readonly ConcurrentCount _parent;
            private int _disposeCount;
            public ScopeImpl(ConcurrentCount parent, long current, long maxCurrent, int threadId)
            {
                Current = current;
                MaxCurrent = maxCurrent;
                ThreadIdOnEnter = threadId;

                _parent = parent;
            }

            public void Dispose()
            {
                if (Interlocked.Increment(ref _disposeCount) == 1)
                {
                    _parent._exit();
                    ThreadIdOnExit = Thread.CurrentThread.ManagedThreadId;
                }
            }
        }

        public IScope Scope()
        {
            var x = Interlocked.Increment(ref _current);
            long m;
            while (x > (m = Interlocked.Read(ref _maxCurrent))
                   && Interlocked.CompareExchange(ref _maxCurrent, x, m) != m)
                ;
            return new ScopeImpl(this, x, m, Thread.CurrentThread.ManagedThreadId);
        }

        private void _exit()
        {
            Interlocked.Decrement(ref _current);
        }
    }
}