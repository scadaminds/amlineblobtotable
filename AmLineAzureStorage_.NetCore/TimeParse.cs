﻿using System;
using System.Globalization;

namespace AmLineAzureStorage
{
    public class TimeParse
    {
        public static DateTimeOffset TimeParseMethod(string dt)
        {

            string timeformat = "ddd MMM dd HH:mm:ss yyyy";

            var rowKeyTime = DateTime.ParseExact(dt, timeformat, CultureInfo.InvariantCulture);
            var rowKey = rowKeyTime.ToUniversalTime();
            return rowKey;
        }
    }
}
