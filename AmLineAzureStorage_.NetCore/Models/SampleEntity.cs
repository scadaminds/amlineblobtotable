﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace AmLineAzureStorage.Models
{
    public class PartitionRowKeyComparer: IEqualityComparer<ITableEntity>
    {
        public static PartitionRowKeyComparer Default { get; set; } = new PartitionRowKeyComparer();
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetHashCode(ITableEntity tableEntity)
            => tableEntity.PartitionKey.GetHashCode() ^ tableEntity.RowKey.GetHashCode();
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Equals(ITableEntity tableEntity1, ITableEntity other)
            => other?.RowKey == tableEntity1?.RowKey && other?.PartitionKey == tableEntity1?.PartitionKey;
    }
    public abstract class SingleInsertEntity<T> : ITableEntity
    {
        private string _partitionKey;
        public string PartitionKey { get => _partitionKey; set { throw new NotSupportedException(); } }
        private string _rowKey;
        public string RowKey { get => _rowKey; set { throw new NotSupportedException(); } }
        DateTimeOffset ITableEntity.Timestamp { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        string ITableEntity.ETag { get => throw new NotImplementedException(); set { } }
        public SingleInsertEntity(string partitionKey, string rowKey)
        {
            _partitionKey = partitionKey;
            _rowKey = rowKey;
        }

        public abstract void ReadEntity(IDictionary<string, EntityProperty> properties, OperationContext operationContext);
        public abstract IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext);
    }

    public abstract class SampleEntity<T>: SingleInsertEntity<T>
    {
        public SampleEntity(string partitionKey, string rowKey, string propertyName, T value): base(partitionKey, rowKey)
        {
            Value = value;
            Name = propertyName;
        }
        public T Value { get; }
        public string Name { get; set; }
        public abstract EntityProperty EntityProperty { get; }
        public override void ReadEntity(IDictionary<string, EntityProperty> properties, OperationContext operationContext)
            => properties[Name] = EntityProperty;

        public override IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext)
            => new Dictionary<string, EntityProperty>() { { Name, EntityProperty } };
    }

    public class SampleDoubleEntity : SampleEntity<double>
    {
        public SampleDoubleEntity(string partitionKey, string rowKey, string propertyName, double value): base(partitionKey, rowKey, propertyName, value) { }

        public override EntityProperty EntityProperty => EntityProperty.GeneratePropertyForDouble(Value);
        public static SampleDoubleEntity FromString(string partitionKey, string rowKey, string propertyName, string value) => new SampleDoubleEntity(partitionKey, rowKey, propertyName, Convert.ToDouble(value));
    }
    public class SampleDateTimeEntity : SampleEntity<DateTimeOffset>
    {
        public SampleDateTimeEntity(string partitionKey, string rowKey, string propertyName, DateTimeOffset value) : base(partitionKey, rowKey, propertyName, value)
        {
        }

        public override EntityProperty EntityProperty => EntityProperty.GeneratePropertyForDateTimeOffset(Value);
        public static SampleDateTimeEntity FromString(string partitionKey, string rowKey, string propertyName, string value) => new SampleDateTimeEntity(partitionKey, rowKey, propertyName, TimeParse.TimeParseMethod(value));
    }
}
