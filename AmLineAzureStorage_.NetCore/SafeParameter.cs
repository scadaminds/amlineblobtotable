﻿using System.Collections.Generic;
using System.Linq;

namespace AmLineAzureStorage
{
    public class SafeParameter
    {
        public static string Replace(string n)
        {
            Dictionary<string, string> replacements = new Dictionary<string, string> { { " ", "_" }, { "-", "_" } };
            return replacements.Aggregate(n, (results, s) => results.Replace(s.Key, s.Value)); ;
        }
    }
}
