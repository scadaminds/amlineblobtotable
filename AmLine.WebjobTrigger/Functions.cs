﻿using Microsoft.Azure.WebJobs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using AmLineAzureStorage;

namespace AmLine.WebjobTrigger
{
    public class Functions
    {
        public static void BlobTrigger([BlobTrigger("csvblob/{name}")] string logMessage, string name, TextWriter log)
        {
            log.WriteLine("BlobTrigger name: {0}", name);
            //Console.WriteLine("Content: {0}", logMessage);
            var nameArray = new string[1];
            nameArray[0] = name;
            AmLineAzureStorage.Program.Main(nameArray);
        }
    }
}
