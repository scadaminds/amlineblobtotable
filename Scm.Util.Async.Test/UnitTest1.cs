using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xunit;

using Scm.Util.Async;

namespace Scm.Util.Async.Test
{
    public class ScanWindowExtensionsTests
    {
        [Fact]
        public async Task ScanWindowByCountShouldInvokeDurationOnExactBoundaries()
        {
            var cnt = 5;
            var itemsSeen = new List<int>();
            var sumsSeen = new List<long>();
            var range = await Observable.Range(0, cnt)
                .ScanSumTo(x => x, (sums, xs) => xs.Do(itemsSeen.Add).Zip(sums.Do(sumsSeen.Add), (a,b) => 1))
                .ToList();
            var inputs = Enumerable.Range(0, cnt).ToList();
            range.Should().BeEquivalentTo(inputs);
            itemsSeen.Should().BeEquivalentTo(inputs);
            var expectedSums = inputs.Select((x, i) => inputs.Take(i+1).Sum()).ToList();
            sumsSeen.Should().BeEquivalentTo(expectedSums);
        }
    }
}
