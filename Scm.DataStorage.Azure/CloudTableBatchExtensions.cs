using System;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading;
using Microsoft.WindowsAzure.Storage.Table;

namespace Scm.DataStorage.Azure
{
    public static class CloudTableBatchExtensions
    {
        public static int DefaultBatchSize = 100;
        /// <summary>
        /// Split <paramref name="entities"/> into batches ready for sending in <see cref="TableBatchOperation"/>s.
        /// </summary>
        /// <param name="entities">Entities to split into batches</param>
        /// <param name="durationSelector">Observable used to commit groups when there is no objects arriving (as <see cref="Observable.GroupByUntil{TSource,TKey,TElement,TDuration}(System.IObservable{TSource},System.Func{TSource,TKey},System.Func{TSource,TElement},System.Func{System.Reactive.Linq.IGroupedObservable{TKey,TElement},System.IObservable{TDuration}},System.Collections.Generic.IEqualityComparer{TKey})"/></param>
        /// <param name="capacity">Max capacity in a batch</param>
        public static IObservable<IGroupedObservable<string, T>> Batch<T, TDuration>(
            this IObservable<T> entities, Func<IGroupedObservable<string, T>, IObservable<TDuration>> durationSelector, int? capacity = null)
            where T : ITableEntity
        {
            IObservable<TDuration> CapacityLimit(IGroupedObservable<string, T> grp) => grp.Skip((capacity ?? DefaultBatchSize) - 1).Select(_ => default(TDuration));
            return durationSelector == null
                ? entities.GroupByUntil(x => x.PartitionKey, CapacityLimit)
                : entities.GroupByUntil(x => x.PartitionKey, grp => CapacityLimit(grp).Amb(durationSelector(grp)));
        }

        /// <inheritdoc cref="Batch{T,TDuration}"/>
        public static IObservable<IGroupedObservable<string, T>> Batch<T>(
            this IObservable<T> entities, TimeSpan? duration = null, int? capacity = null)
            where T : ITableEntity
        {
            Func<IGroupedObservable<string, T>, IObservable<long>> durationSelector = null;
            if (duration != null)
                durationSelector = grp => Observable.Timer(duration.Value);
            return entities.Batch(durationSelector, capacity);
        }

        /// <summary>
        /// Groups <paramref name="entities"/> into <see cref="TableBatchOperation"/> ready for execution against a <see cref="CloudTable"/>.
        /// 
        /// Use the <paramref name="action"/> to specify which operation to by, for example the default: <see cref="TableBatchOperation.InsertOrMerge"/>
        /// <code>(op,e) =&gt; op.InsertOrMerge(e)</code>.
        /// 
        /// For other options, see <see cref="Batch{T}(IObservable{T}, TimeSpan?, int?)"/>
        /// </summary>
        public static IObservable<TableBatchOperation> Batch<T>(
            this IObservable<T> entities, Action<TableBatchOperation, T> action = null, TimeSpan? duration = null, int? capacity = null)
            where T : ITableEntity
        {
            if (action == null)
                action = (op, e) => op.InsertOrMerge(e);
            return entities.Batch(duration, capacity)
                .SelectMany(async (IGroupedObservable<string, T> grp, CancellationToken ct) =>
                    await grp.Aggregate(
                        new TableBatchOperation(),
                        (acc, next) =>
                        {
                            action(acc, next);
                            return acc;
                        }).ToTask(ct).ConfigureAwait(false));
        }
    }
}