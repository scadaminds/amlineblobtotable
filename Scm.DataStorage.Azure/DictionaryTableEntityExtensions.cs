﻿using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage.Table;

namespace Scm.DataStorage.Azure
{
    public static class DictionaryTableEntityExtensions {
        public static DictionaryTableEntity ToTableEntity(this IDictionary<string, EntityProperty> dict, string partitionKey, string rowKey)
            => new DictionaryTableEntity(partitionKey, rowKey, dict);
    }
}