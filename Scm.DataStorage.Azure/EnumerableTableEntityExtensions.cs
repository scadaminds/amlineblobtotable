﻿using System;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage.Table;

namespace Scm.DataStorage.Azure
{
    public static class EnumerableTableEntityExtensions
    {
        public static DictionaryTableEntity ToTableEntity<TSource>(this IEnumerable<TSource> source,
            Func<TSource, string> keyOf, Func<TSource, EntityProperty> valueOf, string partitionKey, string rowKey)
            => new DictionaryOfEnumerable<TSource, string, EntityProperty>(source, keyOf, valueOf).ToTableEntity(partitionKey, rowKey);
        public static DictionaryTableEntity ToTableEntity<TSource>(this IEnumerable<TSource> source,
            Func<TSource, string> keyOf, Func<TSource, object> valueOf, string partitionKey, string rowKey)
            => source.ToTableEntity(keyOf, src => EntityProperty.CreateEntityPropertyFromObject(valueOf(src)), partitionKey, rowKey);
    }
}
