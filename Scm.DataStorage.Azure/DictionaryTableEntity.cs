﻿using System;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Scm.DataStorage.Azure
{
    public class DictionaryTableEntity : ITableEntity
    {
        public DictionaryTableEntity(string partitionKey, string rowKey, IDictionary<string, EntityProperty> dict)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
            _dict = dict;
        }
        private readonly IDictionary<string, EntityProperty> _dict;
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string ETag { get; set; }

        public void ReadEntity(IDictionary<string, EntityProperty> properties, OperationContext operationContext)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext)
        {
            return _dict;
        }
    }
}