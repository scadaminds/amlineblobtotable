using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Scm.DataStorage.Azure
{
    public static class CloudTableExtensions
    {
        /// <summary>
        /// Act on <paramref name="cloudTable"/> in batches configured by <paramref name="action"/>.
        /// </summary>
        public static IObservable<IObservable<IList<TableResult>>> Act<T>(
            this CloudTable cloudTable,
            IObservable<T> entities,
            Action<TableBatchOperation, T> action = null,
            TimeSpan? nagle = null,
            TableRequestOptions tableRequestOptions = null,
            OperationContext operationContext = null,
            int? capacity = null,
            TaskScheduler taskScheduler = null,
            TaskCreationOptions? taskCreationOptions = null)
            where T : ITableEntity
        {
            return entities.Batch(action, nagle, capacity)
                .Select(batch => Observable.FromAsync(async ct =>
                {
                    var t = await Task.Factory.StartNew(async () =>
                                await cloudTable.ExecuteBatchAsync(batch, tableRequestOptions, operationContext, ct),
                            ct, taskCreationOptions ?? TaskCreationOptions.None,
                            taskScheduler ?? TaskScheduler.Current)
                        .ConfigureAwait(false);
                    var r = await t.ConfigureAwait(false);
                    return r;
                }));
        }

        /// <summary>
        /// Perform <see cref="CloudTable.ExecuteAsync(Microsoft.WindowsAzure.Storage.Table.TableOperation)"/> efficiently on all <paramref name="entities"/>
        /// </summary>
        public static IObservable<IObservable<IList<TableResult>>> InsertOrMerge<T>(
            this CloudTable cloudTable,
            IObservable<T> entities,
            TimeSpan? nagle = null,
            TableRequestOptions tableRequestOptions = null,
            OperationContext operationContext = null,
            int? capacity = null,
            TaskScheduler taskScheduler = null,
            TaskCreationOptions? taskCreationOptions = null)
            where T : ITableEntity
            => cloudTable.Act(entities, (op, e) => op.InsertOrMerge(e), nagle, tableRequestOptions, operationContext,
                capacity, taskScheduler, taskCreationOptions);

        /// <summary>
        /// Perform <see cref="CloudTable.ExecuteAsync(Microsoft.WindowsAzure.Storage.Table.TableOperation)"/> efficiently on all <paramref name="entities"/>
        /// 
        /// Applies <paramref name="maxConcurrent"/> concurrent requests using <paramref name="scheduler"/> and cancellable by <paramref name="cancellationToken"/>.
        /// 
        /// The <paramref name="capacity"/> and <paramref name="nagle"/> can be used to decide the batch-sizes set.
        /// 
        /// Specific options to the <paramref name="cloudTable"/> batch operation can be passed in through <paramref name="tableRequestOptions"/> and <paramref name="operationContext"/>.
        /// 
        /// As a workaround for <see cref="CloudTable.ExecuteBatchAsync(Microsoft.WindowsAzure.Storage.Table.TableBatchOperation)"/>
        ///  blocking execution (contrary to what one would believe from the name and signature) you can provide a <paramref name="taskScheduler"/> and <paramref name="taskCreationOptions"/>
        /// that will be used to execute that call.
        /// 
        /// </summary>
        /// <returns>Complete list of <see cref="TableResult"/> returned</returns>
        public static async Task<IList<TableResult>> InsertOrMergeAsync<T>(
            this CloudTable cloudTable,
            IEnumerable<T> entities,
            int? maxConcurrent = null,
            CancellationToken? cancellationToken = null,
            IScheduler scheduler = null,
            int? capacity = null,
            TimeSpan? nagle = null,
            TableRequestOptions tableRequestOptions = null,
            OperationContext operationContext = null,
            TaskScheduler taskScheduler = null,
            TaskCreationOptions? taskCreationOptions = null
        )
            where T : ITableEntity
        {
            return await cloudTable.InsertOrMerge(entities.ToObservable(scheduler ?? Scheduler.Default), nagle, tableRequestOptions,
                    operationContext, capacity, taskScheduler, taskCreationOptions)
                .Merge(maxConcurrent ?? Math.Max(Environment.ProcessorCount + 1, 20))
                .SelectMany(x => x)
                .ToList()
                .ToTask(cancellationToken ?? default(CancellationToken))
                .ConfigureAwait(false);
        }

    }
}