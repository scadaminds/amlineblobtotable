﻿using System;
using System.IO;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Scm.DataStorage.Azure
{
    public static class CloudBlockBlobExtensions
    {
        public static Encoding DefaultEncoding { get; set; } = Encoding.UTF8;
        public static int DefaultBufferSize { get; set; } = 1024 * 1024;
        public static bool DefaultDetectEncodingFromByteOrderMarks { get; set; } = false;
        public static bool DefaultLeaveOpen { get; set; } = true;
        public static IObservable<string> Lines(this CloudBlockBlob b, Encoding encoding = null, int? bufferSize = null, bool? detectEncodingFromByteOrderMarks = null, bool? leaveOpen = null, Func<CancellationToken, Task> backPressure = null)
            =>
                Observable.Using(
                    async ct1 => new StreamReader(await b.OpenReadAsync().ConfigureAwait(false),
                        encoding??DefaultEncoding??Encoding.Default, 
                        bufferSize: bufferSize ?? DefaultBufferSize, 
                        detectEncodingFromByteOrderMarks: detectEncodingFromByteOrderMarks ?? DefaultDetectEncodingFromByteOrderMarks,
                        leaveOpen: leaveOpen ?? DefaultLeaveOpen),
                    async (r, ct2) =>
                    {
                        await Task.Yield();
                        return Observable.Create<string>(async (obs, ct) =>
                        {
                            try
                            {
                                for (var l = await r.ReadLineAsync();
                                    !ct.IsCancellationRequested && l != null;
                                    l = await r.ReadLineAsync())
                                {
                                    obs.OnNext(l);
                                    if (backPressure == null)
                                        continue;
                                    try
                                    {
                                        await backPressure(ct).ConfigureAwait(false);
                                    }
                                    catch when (ct.IsCancellationRequested)
                                    {
                                        // Swallow exceptions if cancellation has be requested.
                                        // It is just an unsubscription
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                obs.OnError(ex);
                            }
                            finally
                            {
                                obs.OnCompleted();
                            }
                        });
                    });
    }
}