using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scm.Util.Async
{
    public static class ParallelCompose
    {
        /// <summary>
        /// Aggregates <paramref name="source"/> using <paramref name="aggregate"/>, applying await in the proper way.
        /// 
        /// Corresponds to <see cref="Enumerable.Aggregate{TSource}"/>
        /// </summary>
        public static Task<TAgg> AggregateAsync<TSource, TAgg>(
            this IEnumerable<Task<TSource>> source,
            TAgg accumulator,
            Func<TAgg, TSource, TAgg> aggregate)
            => source.Aggregate(Task.FromResult(accumulator), async (acc, next) =>
                aggregate(await acc.ConfigureAwait(false), await next.ConfigureAwait(false)));

        /// <summary>
        /// Aggregates the sum of <paramref name="source"/> results
        /// </summary>
        public static Task<int> SumAsync(this IEnumerable<Task<int>> source) => source.AggregateAsync(0, (acc, next) => acc + next);
    }
}