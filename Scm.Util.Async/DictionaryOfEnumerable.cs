﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Scm.DataStorage.Azure
{
    public class DictionaryOfEnumerable<TSource, TKey, TValue> : IDictionary<TKey, TValue>
    {
        public IEnumerable<TSource> Source { get; }
        public Func<TSource, TKey> KeyOf { get; }
        public Func<TSource, TValue> ValueOf { get; }
        public DictionaryOfEnumerable(IEnumerable<TSource> source, Func<TSource, TKey> keyOf, Func<TSource, TValue> valueOf)
        {
            Source = source;
            KeyOf = keyOf;
            ValueOf = valueOf;
        }

        protected class PairEnumerable : IEnumerable<KeyValuePair<TKey, TValue>>
        {
            public DictionaryOfEnumerable<TSource, TKey, TValue> Parent;

            public PairEnumerable(DictionaryOfEnumerable<TSource, TKey, TValue> parent)
            {
                Parent = parent;
            }

            public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
                => Parent.Source.Select(src => KeyValuePair.Create(Parent.KeyOf(src), Parent.ValueOf(src))).GetEnumerator();
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => new PairEnumerable(this).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            foreach (var src in Source)
                array[arrayIndex++] = KeyValuePair.Create(KeyOf(src), ValueOf(src));
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get
            {
                if (Source is ICollection collection)
                    return collection.Count;
                throw new NotImplementedException("Can only count ICollection sources");
            }
        }

        public bool IsReadOnly => true;
        public void Add(TKey key, TValue value)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(TKey key)
        {
            throw new NotImplementedException();
        }

        public bool Remove(TKey key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            throw new NotImplementedException();
        }

        public TValue this[TKey key]
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public ICollection<TKey> Keys => new CollectionOfEnumerable<TKey>(Source.Select(KeyOf));
        public ICollection<TValue> Values => new CollectionOfEnumerable<TValue>(Source.Select(ValueOf));
    }
}