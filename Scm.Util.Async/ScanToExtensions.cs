using System;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace Scm.Util.Async
{
    public static class ScanToExtensions
    {
        /// <summary>
        /// Scans through <paramref name="source"/> performing <see cref="Observable.Scan{TSource,TAccumulate}"/>.
        /// Invokes <paramref name="aggregated"/> on the accumulation observable.
        /// </summary>
        public static IObservable<T> ScanTo<T, TAccumulate, TSampler>(
            this IObservable<T> source,
            TAccumulate seed, Func<TAccumulate, T, TAccumulate> accumulate,
            Func<IObservable<TAccumulate>, IObservable<T>, IObservable<TSampler>> aggregated)
        {
            var src = source.Publish().RefCount();
            //return src.WithLatestFrom(aggregated(src.Scan(seed, accumulate), src), (s, a) => s);
            var agg = src.Scan(seed, accumulate).Publish().RefCount();
            var tagg = aggregated(agg, src);
            return src.WithLatestFrom(tagg.StartWith(default(TSampler)), (s, a) => s);
        }
        public static IObservable<T> ScanSumTo<T,TSampler>(
            this IObservable<T> source,
            Func<T, long> countOf,
            Func<IObservable<long>, IObservable<T>, IObservable<TSampler>> aggregated)
            => source.ScanTo(0L, (a,v) => a+countOf(v), aggregated);

        public static IObservable<T> ScanSumTo<T, TSampler>(
            this IObservable<T> source,
            Func<T, long> countOf,
                Func<IObservable<long>, IObservable<TSampler>> aggregated)
            => source.ScanSumTo(countOf, (a, v) => aggregated(a));
    }

    public static class TimeSinceExtensions
    {
        public static IObservable<TimeInterval<T>> TimeIntervalSince<T>(this IObservable<T> source,
            IScheduler scheduler = null,
            DateTimeOffset? now = null)
        {
            var relativeTo = now ?? scheduler?.Now ?? Scheduler.Default.Now;
            return (scheduler == null ? source.Timestamp() : source.Timestamp(scheduler)).Select(x =>
                new TimeInterval<T>(x.Value, x.Timestamp - relativeTo));
        }
    }
}