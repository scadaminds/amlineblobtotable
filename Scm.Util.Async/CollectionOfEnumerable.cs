﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Scm.DataStorage.Azure
{
    public class CollectionOfEnumerable<TSource>: ICollection<TSource> {
        public IEnumerable<TSource> Source { get; }
        public CollectionOfEnumerable(IEnumerable<TSource> source)
        {
            Source = source;
        }

        public IEnumerator<TSource> GetEnumerator() => Source.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public void Add(TSource item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(TSource item)
        {
            if (Source is ICollection<TSource> c)
                return c.Contains(item);
            throw new NotImplementedException("Can only decide contains on ICollection Source");
        }

        public void CopyTo(TSource[] array, int arrayIndex)
        {
            if(Source is ICollection<TSource> c)
                c.CopyTo(array, arrayIndex);
            else
                foreach (var src in Source)
                    array[arrayIndex++] = src;
        }

        public bool Remove(TSource item)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get
            {
                if (Source is ICollection<TSource> c)
                    return c.Count;
                throw new NotImplementedException("Count is only implemented on ICollection Source");

            }
        }

        public bool IsReadOnly => true;
    }
}