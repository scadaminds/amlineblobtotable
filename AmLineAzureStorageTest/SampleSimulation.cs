using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading;
using System.Threading.Tasks;
using AmLineAzureStorage;
using FluentAssertions;
using Microsoft.WindowsAzure.Storage.Table;
using Scm.DataStorage.Azure;
using Scm.Util.Async;
using Xunit;
using Xunit.Abstractions;

namespace AmLineAzureStorageTest
{
    public class SampleSimulation: PerformanceCloudStorageTest, IClassFixture<SampleSimulation.SampleParams>
    {
        public class SampleParams : TestParams
        {
            public SampleParams() : base(new TestEntityProducer().TestEntities(71, 1, 200)) { }
        }
        protected TestParams Params;
        public SampleSimulation(ITestOutputHelper output, SampleParams sampleParams) : base(output)
        {
            Params = sampleParams;
        }
        [Fact]
        public async Task ObservableEntitiesThroughExtension()
        {
            await Prepare().ConfigureAwait(false);
            var sw = Stopwatch.StartNew();
            var got = await CloudTable.InsertOrMerge(Params.Entities.ToObservable())
                .Merge(20)
                .ToList().ToTask(CancellationToken).ConfigureAwait(false);
            sw.Stop();
            var result = new TestResult(Params, sw);
            result.TraceTo(Output.WriteLine);
        }
        [Fact]
        public async Task EnumerableEntitiesThroughExtension()
        {
            await Prepare().ConfigureAwait(false);
            var sw = Stopwatch.StartNew();
            var got = await CloudTable.InsertOrMergeAsync(Params.Entities, 20, CancellationToken).ConfigureAwait(false);
            sw.Stop();
            var result = new TestResult(Params, sw);
            got.Should().HaveCount(Params.EntityCount);
            result.TraceTo(Output.WriteLine);
        }
    }
}