using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.WindowsAzure.Storage.Table;
using Xunit;
using Xunit.Abstractions;

namespace AmLineAzureStorageTest
{
    [Collection("Performance")]
    public abstract class PerformanceCloudStorageTest : CloudStorageTestBase {
        protected ITestOutputHelper Output { get; }

        protected PerformanceCloudStorageTest(ITestOutputHelper output)
        {
            Output = output;
        }
        public class TestParams
        {
            public int NumberOfPartitions { get; }
            public int NumberOfProperties { get; }
            public int NumberOfRows { get; }
            public int EntityCount { get; }
            public List<List<ITableEntity>> BatchedEntities { get; }
            public IEnumerable<ITableEntity> Entities => BatchedEntities.SelectMany(x => x);
            public TestParams(IEnumerable<List<ITableEntity>> batchedEntities)
            {
                BatchedEntities = batchedEntities.ToList();
                NumberOfPartitions = BatchedEntities.SelectMany(x => x.Select(e => e.PartitionKey)).Distinct().Count();
                NumberOfRows = BatchedEntities.SelectMany(x => x.Select(e => e.RowKey)).Distinct().Count();
                NumberOfProperties = BatchedEntities.Sum(x => x.Sum(e => e.WriteEntity(null).Count));
                EntityCount = BatchedEntities.Sum(x => x.Count);
            }

            public TestParams(int numberOfPartitions, int numberOfProperties, int numberOfRows)
            {
                NumberOfPartitions = numberOfPartitions;
                NumberOfProperties = numberOfProperties;
                NumberOfRows = numberOfRows;
            }

            public override string ToString() =>
                $"#part: {NumberOfPartitions}, #rows: {NumberOfRows}, #prop: {NumberOfProperties}";
        }

        public class TestResult
        {
            public TestResult(TestParams testParams, Stopwatch stopwatch)
            {
                TestParams = testParams;
                Stopwatch = stopwatch;
            }

            public TestParams TestParams { get;  }
            public Stopwatch Stopwatch { get; }
            public double EntityPerSec => TestParams.EntityCount / Stopwatch.Elapsed.TotalSeconds;
            public double PropertyPerSec => TestParams.NumberOfProperties / Stopwatch.Elapsed.TotalSeconds;

            public void TraceTo(Action<string, object[]> output)
            {
                output("Saved {0} entities, {1} in {2}", new object[] {TestParams.EntityCount, TestParams, Stopwatch.Elapsed});
                output("{0:f2} entities/sec, {1:f2} properties/sec", new object[]{ EntityPerSec, PropertyPerSec });

            }
        }

        public virtual async Task<TestResult> TestImplementation(TestParams testParams, Func<IEnumerable<List<ITableEntity>>, Task<int>> impl)
        {
            await Prepare();
            var sw = Stopwatch.StartNew();
            var entityCount = await impl(testParams.BatchedEntities).ConfigureAwait(false);
            sw.Stop();
            var result = new TestResult(testParams, sw);
            result.TraceTo(Output.WriteLine);
            entityCount.Should().Be(testParams.EntityCount);
            return result;
        }
    }
}