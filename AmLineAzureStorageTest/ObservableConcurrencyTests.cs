using System;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using AmLineAzureStorage.Helpers;
using FluentAssertions;
using Xunit;

namespace AmLineAzureStorageTest
{
    public class ObservableConcurrencyTests
    {
        [Fact]
        public async Task ConurrencyOfMergeTest()
        {
            var cc = new ConcurrentCount();
            var maxConcurrency = 50;
            var inputCount = 100;
            var got = await Enumerable.Range(0, inputCount)
                .Select(i => Observable.FromAsync(async ct =>
                {
                    using (var scope = cc.Scope())
                    {
                        await Task.Delay(TimeSpan.FromMilliseconds(10), ct).ConfigureAwait(false);
                        return scope;
                    }
                }))
                .Merge(maxConcurrency)
                .ToList()
                .ToTask()
                .ConfigureAwait(false);
            got.Should().NotContain(x => x.MaxCurrent > maxConcurrency)
                .And.Contain(x => x.Current >= maxConcurrency / 2)
                .And.Contain(x => x.ThreadIdOnEnter != x.ThreadIdOnExit);
            got.Where(x => x.Current >= maxConcurrency / 2).Should().HaveCountGreaterOrEqualTo(inputCount/2);
        }
    }
}