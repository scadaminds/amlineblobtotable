using AmLineAzureStorage;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AmLineAzureStorageTest
{
    public class CloudStorageTestBase
    {
        protected CancellationToken CancellationToken => CancellationToken.None;
        protected virtual async Task Prepare()
        {
            ThreadPool.SetMinThreads(1024, 256);
            var tableServicePoint = ServicePointManager.FindServicePoint(StorageAccount.TableEndpoint);
            tableServicePoint.UseNagleAlgorithm = false;
            tableServicePoint.Expect100Continue = false;
            tableServicePoint.ConnectionLimit = 1000;
            await Task.Yield();
        }
        private CloudStorageAccount _storageAccount;
        protected CloudStorageAccount StorageAccount => _storageAccount ?? (_storageAccount = new MyConfig().CloudStorageAccount);
        private CloudTableClient _cloudTableClient;
        protected CloudTableClient CloudTableClient => _cloudTableClient ?? (_cloudTableClient = StorageAccount.CreateCloudTableClient());
        public static string DefaultTableName = "testTable2";
        protected String TableName => DefaultTableName;

        private CloudTable _table;
        public virtual CloudTable CloudTable => _table ?? (_table = CloudTableClient.GetTableReference(TableName));
    }
}

