using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace AmLineAzureStorageTest
{
    public class TestEntityProducer
    {
        public IEnumerable<List<ITableEntity>> TestEntities(int numberOfPartitions, int numberOfProperties, int numberOfRows, string rowPrefix = null)
        {
            if (rowPrefix == null)
                rowPrefix = Guid.NewGuid().ToString();
            IDictionary<string, EntityProperty> properties = Enumerable.Range(0, numberOfProperties).ToDictionary(x => $"p{x}", x => new EntityProperty((float)x));

            var batchSize = 100;
            foreach (var partition in Enumerable.Range(0, numberOfPartitions).Select(i => $"{i}"))
            {
                for (int restRows = numberOfRows, batch = 0; restRows > 0; restRows -= batchSize, ++batch)
                {
                    var batchRows = Math.Min(restRows, batchSize);
                    yield return
                        Enumerable.Range(0, batchRows)
                            .Select(i => new DynamicTableEntity(partition, $"{rowPrefix}-{batch}-{i}", null, properties))
                            .Cast<ITableEntity>()
                        .ToList();

                }
            }
        }

    }
}